1.- Menciona cuales son las regiones principales de memoria que maneja Java
	Stack	Se usa para la asignacion de memoria estatica durante la ejecucion de un hilo.
	Heap	Se usa para la asignación de memoria dinámica para objetos Java y clases en tiempo de ejecución.

2.- Menciona las caracteristicas con las que cumple el garbage collector.
	Es una aplicacion que se encarga de gestionar el Heap, liberando la memoria de aquellos objetos que ya no se utilizan.

3.- Cual es el Garbage por collector por default que tiene establecido la JVM en java8
	Parallel
	
4.- Cuales son las diferencias entre el garbage collector serial y el garbage collector paralell
	El GC Serial utiliza un solo hilo para la recoleccion de basura, el GC Parallel utiliza varios hilos.

5.- En cuantas regiones diferentes se divide el heap?
	En tres regiones:
	Young Generation (Eden, Survivor Space S0 y S1)
	Old Generation
	Permament Generation

6.- En que región del heap es donde se almacenan por primera vez las instancias nuevas de objetos?
	En el Eden

7.- Menciona en que consiste un Memory Leak
	Es cuando hay objetos en el heap que no se usan pero el GC no puede eliminarlos de la memoria.

8.- En que consiste el Memory Leak causado por campos estaticos y como se puede prevenir?
	Es cuando se usan demasiadas variables static y estas llenan la memoria, ya que el GC no las puede eliminar. Se deben usar sólo cuando sea realmente necesario.

9.- Bajo que escenarios se presenta la siguiente excepción java.lang.StackOverflowError ?
	Este error ocurre cuando se llena la parte de memoria Stack. Puede ocurrir por ejemplo cuando se llama recursivamente a un método de manera indefinida.
	
10.- Bajo que escenarios se puede producir un java.lang.OutOfMemoryError: Javaheap space ?
	Puede producirse cuando se llena el heap y el GC no es capaz de limpiar todos los recursos, por lo que se llena la memoria. 
	Por ejemplo cuando no se cierran recursos correctamente, al estar abiertos se estan "usando" y mientras mas se crean menos espacio hay disponible.

11.- Menciona la proporcion de memoria mínima y máxima por default que toma la JVM, acorde a la memoria RAM instalada
	Mínimo 1/64 de la memoria total y máximo 1/4.

12 .- Que argumentos utilizarias para establecer la ejecución de un archivo jar conlas siguientes caracteristicas:
	100 MB como heap mínimo
	2 GB como heap maximo
	Impresion de detalles del GC
	Salida a un archivo de los detalles del garbage collector
	Ejecución del garbage collector de tipo paralell con dos threads

	-Xms100m -Xmx2048m -XX:+PrintGCDetails -Xloggc:/tmp/gc-info.log -XX:+UseParallelGC -XX:ParallelGCThreads=2
